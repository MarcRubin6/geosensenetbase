clear all; close all; clc;

for i=1:9
    s = sprintf('%d.dat24',i);
    fid = fopen(s,'rb');
    d = fread(fid, [inf],'uint32');
%       d = dlmread(s);
    data{i} = d;
    fclose(fid);
end

figure;
hold on;
m = 0;
for i=9:-1:1
    c = data{i};
    c = c + m;
    plot(c);
    m = max(c);
end