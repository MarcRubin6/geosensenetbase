#include <GeoMoteShield.h>
#include <SPI.h>
#include <Streaming.h>

#define MOTE_ID 1

//GeoMoteShield object initializes everything
// also calibrates the adc, so avoid input during powerup
GeoMoteShield gms;

void setup()
{
  Serial.begin(57600);
  Serial.flush();

  //set defaults to 500 Hz, 16-bit precision, Gain of 1
  gms.adc->setFrequency(F500);
  gms.setHighPrecision(false);
  gms.adc->setGain(GAIN_1);
}

void loop()
{
  char c;
  int n;
  
  //wait for data to be available
  if(Serial.available()){
    c = Serial.read();
    
    // 'R' command to start sampling
    if(c == 'R') gms.start_sampling();
    
    // 'C' command calibrates
    else if(c == 'C') gms.adc->calibrate();
    
    // 'S' command stops sampling
    else if(c == 'S') gms.stop_sampling();
    
    //'P' to change precision
    else if(c == 'P'){
      delay(2);
      c = Serial.read();
      
      // 'L' for 16 bit, 'H' for 24 bit
      if(c == 'L') gms.setHighPrecision(false);
      else if(c == 'H') gms.setHighPrecision(true);
    }
    
    //'G#' to change gain
    else if(c == 'G'){
      delay(2);
      c = Serial.read();
      n = c - '0';
      
      switch(n){
        case 1: gms.adc->setGain(GAIN_1); break; 
        case 2: gms.adc->setGain(GAIN_2); break; 
        case 3: gms.adc->setGain(GAIN_4); break; 
        case 4: gms.adc->setGain(GAIN_8); break; 
        case 5: gms.adc->setGain(GAIN_16); break; 
        case 6: gms.adc->setGain(GAIN_32); break; 
        case 7: gms.adc->setGain(GAIN_64); break; 
        case 8: gms.adc->setGain(GAIN_128); break; 
      }
    }
    
    // 'F#' to change frequency
    else if(c == 'F'){
      delay(2);
      c = Serial.read();
      n = c - '0';
      switch(n){
        case 1: gms.adc->setFrequency(F100); break; 
        case 2: gms.adc->setFrequency(F250); break;
        case 3: gms.adc->setFrequency(F500); break;
        case 4: gms.adc->setFrequency(F1000); break;
        case 5: gms.adc->setFrequency(F2000); break;
        case 6: gms.adc->setFrequency(F4000); break;
      }    
    }
    
    //'T#' to start transmitting, # is MOTE_ID
    else if(c == 'T'){
      delay(2);
      c = Serial.read();
      n = c - '0';
      //if(n == MOTE_ID){
      //used for debugging 
      if(n >= 1 && n <= 9){
         gms.transmit_data();
      } 
    }
  
    //'I#' to identify mote. # is MOTE_ID
    else if(c == 'I'){
      delay(2);
      c = Serial.read();
      n = c - '0';
      //if(n == MOTE_ID){
      if(n >= 1 && n <= 9){ //debugging
        Serial << "MOTE " << MOTE_ID << " OK!" << endl;
      } 
    }
  }
}


