#!/usr/bin/python

import serial
import sys
import time
import threading
import os

#thread function to receive input
def receiver(s,f,m):
    print "Getting data from mote: " + str(m) + "...";
    s.flush();
    s.flushInput();
    s.flushOutput();
    while(run_thread):
        while(s.inWaiting() > 0):
            c = s.read();
            f.write(c);
            f.flush();

#thread read_line function
def read_line(s):
    s.flush();
    s.flushInput();
    s.flushOutput();
    while(run_thread):
        #wait for enough characters
        while(s.inWaiting() > 3):
            msg = s.readline().strip('\n');
            sys.stdout.write(msg);
            sys.stdout.flush();

#get command line args
if (len(sys.argv) != 4):
    print >> sys.stderr, "usage: ./GeoSenseNetBase /dev/tty.usbXXXX baud_rate numMotes";
    exit(1);

try:
    # open serial port
    serial_port = serial.Serial(sys.argv[1], int(sys.argv[2]), timeout=0);

except Exception as err:
    print >> sys.stderr, 'ERROR: COULD NOT OPEN SERIAL PORT';
    print >> sys.stderr, err;
    exit(1);

#define number of motes and output file name
num_motes = int(sys.argv[3]);
baud_rate = int(sys.argv[2]);
time_str = 'default';

#keep track of threads
run_thread = False;

#dictionary of geomoteshield codes..
codes = {'F100':'F1', 'F250':'F2', 'F500':'F3', 'F1000':'F4', 'F2000':'F5', 'F4000':'F6', \
    'G1':'G1', 'G2':'G2', 'G4':'G3', 'G8':'G4', 'G16':'G5', 'G32':'G6', 'G64':'G7', 'G128':'G8',\
    'P16':'PL', 'P24':'PH'}

#define default values
frequency = 500;
precision = 16;
gain = 1;

print "Setting default mote parameters (500 Hz, 16-bits, Gain 1)..."

#give radio enough time to get establish connection
time.sleep(3);
serial_port.flushInput();
serial_port.flushOutput();

#check to see if all N motes are present
for m in range(1,num_motes+1):

    t = threading.Thread(target=read_line, args=(serial_port,));
    run_thread = True;
    t.setDaemon(True);
    t.start();
    
#    print "Pinging mote " + str(m) + "...";
    sys.stdout.write("Pinging mote " + str(m) + "...");
    sys.stdout.flush();
    
    # ping the mote
    serial_port.write('I' + str(m));
    
    # wait 500 milliseconds
    time.sleep(0.500);
    
    # kill and join thread
    run_thread = False;
    t.join();
    sys.stdout.write('\n');

#transmit default values 500 Hz -> F3
serial_port.write('F3');
time.sleep(0.1);

serial_port.write('G1');
time.sleep(0.1);

serial_port.write('PL');
time.sleep(0.1);

#serial_port.write('C');
#time.sleep(0.1);

t1 = time.time();
t2 = time.time();

# wait for keyboard input
try:
    while True:
        print "=====================================================================";
        print "Waiting for keyboard command...";
        print "R to start"
        print "S to stop";
        print "C to recalibrate";
        print "P# to set precision (16 or 24), e.g. P16"
        print "G# to set gain (1,2,4,8,16,32,64, or 128), e.g., G64";
        print "F# to set sample frequency (100,250,500,1000,2000,or 4000), e.g., F500";
        print "=====================================================================";
        
        #get user input
        input_str = sys.stdin.readline().strip('\n');
        
        #check to see if input is a command for frequency, gain, or precsion
        if(codes.has_key(input_str)):
            command = codes[input_str];
            
            # 'F', change frequency 100->F1, 250->F2, 500->F3, 1000->F4, 2000->F5, 4000->F6
            if 'F' in command:
                frequency = int(input_str.strip('F'));
                print "Changing sampling frequency to " + str(frequency);
            
            #'G', change gain
            elif 'G' in command:
                gain = int(input_str.strip('G'));
                print "Changing gain to " + str(gain);
            
            #'P', change precision
            elif 'P' in command:
                precision = int(input_str.strip('P'));
                print "Changing precision to " + str(precision);
        
            else:
                pass;
                    
            #transmit command
            serial_port.write(command);
            time.sleep(0.1);
    
        # 'C', calibrate motes
        elif(input_str == 'C'):
            print "Calibrating motes...";
            serial_port.write('C');
            time.sleep(0.1);
    
        # 'R', record time, transmit R to start motes
        elif(input_str == 'R'):
            print "Staring motes...";
            
            serial_port.flushInput();
            serial_port.flushOutput();
            serial_port.write('R');
            time.sleep(0.1);
            
            time_str = time.strftime("%m_%d_%Y_%H_%M_%S_", time.localtime());
            t1 = time.time();
            
            log_file = open(time_str + 'log.txt','w');
            log_file.write('frequency          = ' + str(frequency) + '\n');
            log_file.write('gain               = ' + str(gain) + '\n');
            log_file.write('precision          = ' + str(precision) + '\n');
            log_file.close();
    
        # 'S', transmit 'S' to stop motes
        elif(input_str == 'S'):
            print "Stopping motes...";
            
            # clear serial buffer
            serial_port.flushInput();
            serial_port.flushOutput();
            
            serial_port.write('S');
            time.sleep(0.1);
            
            #calculate wait time to receive expected data.
            if(precision == 24):
                p = 32;
            else:
                p = 16;
            t2 = time.time();
            delay = t2 - t1;
            wait_time = (delay * frequency * p) / baud_rate;
            if(wait_time > 6):
                wait_time = 6;

            #create files
            for m in range(1,num_motes+1):
                try:
                    fName = time_str + str(m) + '.dat' + str(precision);
                    fid = open(fName, 'wb');

                except IOError as err:
                    print >> sys.stderr, "ERROR: could not open " + fName
                    print >> sys.stderr, err;
                    exit(1);

                # transmit T#
                serial_port.flushInput();
                serial_port.flushOutput();
                time.sleep(0.1);

                # spawn receiver thread with file name
                t = threading.Thread(target=receiver, args=(serial_port,fid,m,));
                run_thread = True;
                t.setDaemon(True);
                t.start();

                #tell mote to start transmitting
                serial_port.write('T' + str(m));

                # wait 7 seconds (~5.5 seconds for 32768 bytes..)
                #time.sleep(7);
                time.sleep(wait_time+1.3)

                # kill and join thread
                run_thread = False;
                t.join();
                fid.close();

                #create simlink for easy Matlab
                os.system('ln -f -s ' + fName + ' ' + str(m) + '.dat' + str(precision));

        else:
            print "unknown input..";

except KeyboardInterrupt:
    print "Exiting..."
    serial_port.close();
    exit(0);