clear all; close all; clc;
N=9;
figure; hold on;
for i=1:N
    s = sprintf('%d.dat16',i);
    fid = fopen(s,'rb');
    d = fread(fid, [inf],'uint16');
%       d = dlmread(s);
    data{i} = d;
    plot(d);
    fclose(fid);
end

figure;
hold on;
m = 0;
for i=N:-1:1
    c = data{i};
    c = c + m;
    plot(c);
    m = max(c);
end